import itertools
import numpy as np
import re 
import os
import spacy
from nltk.tree import Tree
from spacy.tokenizer import Tokenizer


def token_format(token, type_="orth_"):
    """
    The avaliable types are text, tag_, dep_ and orth_
    """
    return getattr(token, type_)

def to_nltk_tree(node, t_="dep_"):
    """
    USAGE
        >>> print([sent.root for sent in doc.sents])
        >>> tree = [to_nltk_tree(sent.root) for sent in doc.sents] # The first item in the list is the full tree
        >>> for t in tree:
        >>>     print(t)
        >>>     t.draw()
        >>> [print(d.text, d.pos_, d.dep_) for d in doc]
    """
    if node.n_lefts + node.n_rights > 0:
        return Tree(token_format(node, type_=t_), [to_nltk_tree(child) for child in node.children])
    else:
        return token_format(node, type_=t_)

def dependency_parser(sentence_tkns, ner_tkns):
    sentence = " ".join(sentence_tkns)

    """
    OLD CogrOO parser
    # cogroo dependency analyzer
    # COGROO = Cogroo.Instance() # transform in a global variable
    # dependency = COGROO.chunk_tag(sentence).replace("_"," ")
    # postags = COGROO.pos_tag(sentence)
    # cogroo_remove_brackets()
    """

    # spacy dependency analyzer
    nlp = spacy.load("pt_core_news_sm")
    doc = nlp(sentence)
    dependency = [(d.text, d.dep_) for d in doc]
    dep_triplets = []
    print("\n\n"+sentence)
    # print(sentence_tkns, ner_tkns)

    token = None
    for token in doc:
        dep_triplets.append((token, token.dep_, token.head))

    token.set_extension("ner", default="O") # force=True force was a spacy 2.1 feature
    
    diff = 0
    word1 = word2 = ""
    for token in doc:
        nerid = 0
        if diff != 0:
            nerid = diff
        if word1 == "":
            word1 = "".join([ch for ch in token.text if ch != " "])
        if word2 == "":
            word2 = "".join([ch for ch in sentence_tkns[token.i+nerid] if ch != " "])

        #print(len(ner_tkns), token.i+nerid)
        ner_ = ner_tkns[token.i+nerid]
        if word1 != word2:
            dff = len(word1) - len(word2)
            if dff > 0:
                nerclass = []
                while dff > 0 and token.i+nerid < len(ner_tkns):
                    nerclass.append(ner_)
                    nerid = nerid+1
                    word2 = word2+"".join([ch for ch in sentence_tkns[token.i+nerid] if ch != " "])
                    dff = len(word1) - len(word2)
                diff = nerid
                nerclass = list(set(nerclass))
                if len(nerclass) > 0:
                    token._.ner = [c for c in nerclass if c != "O"][0] # future implementation of multiclass annotations
                else:
                    token._.ner = nerclass[0]
            elif dff < 0:
                if word1 == token.text:
                    diff = diff-1
                else:
                    word1 = word1+"".join([ch for ch in token.text if ch != " "])
                    dff = len(word1) - len(word2)
                    if word1 == word2:
                        word1 = ""
                        word2 = ""
                    else:
                        diff = diff-1
                token._.ner = ner_
            else:
                token._.ner = ner_
            
            if word1 == word2 or dff == 0:
                word1 = ""
                word2 = ""
        else:
            word1 = ""
            word2 = ""
            token._.ner = ner_

    # print([(tk.text, tk._.ner) for tk in doc if tk._.ner != "O"])
       
    return dep_triplets, doc

def relation_extraction(triplets, doc):
    sentence = " ".join([token.text for token in doc])
    token = doc[0]
    token.set_extension("rel", default="O", force=True)    
    """
    SPECIFIC DEP. CLASSES FOR ARG1-REL-ARG2 TRIPLE
    """
    nsubj = [((tk1_, dep_, tk2_)) for tk1_, dep_, tk2_ in triplets if dep_ == "nsubj"]
    nsubjpass = [((tk1_, dep_, tk2_)) for tk1_, dep_, tk2_ in triplets if dep_ == "nsubj:pass"]
    # ner = [((tk1_, dep_, tk2_)) for tk1_, dep_, tk2_ in triplets if tk1_._.ner != "O" or tk2_._.ner != "O"]
    # nentity = [(tk1_, tk1_._.ner) if tk1_._.ner != "O" else tk2_ for tk1_, dep_, tk2_ in ner]
    print(nsubj+nsubjpass)
    for maintk1, maindep, maintk2 in (nsubj+nsubjpass):
        # print(maintk1.tag_, " - Um ",maintk2.tag_, "Dois")
        if "|N|" in maintk1.tag_ or "PROPN" in maintk1.tag_:
            doc[maintk1.i]._.rel = "subj"
        else:
            print("|V|" in maintk2.tag_)
            if "|V|" in maintk2.tag_:
                doc[maintk2.i]._.rel = "rel"
        if "|N|" in maintk2.tag_ or "PROPN" in maintk2.tag_:
            doc[maintk2.i]._.rel = "subj"
        else:
            if "|V|" in maintk1.tag_:
                doc[maintk1.i]._.rel = "rel"

        relations = ["expl", "aux:pass", "dobj", "cop", "aux", "neg"]
        rel_triplets = [((tk1_, dep_, tk2_)) for tk1_, dep_, tk2_ in triplets if dep_ in relations and (tk1_ in [maintk1, maintk2] or tk2_ in [maintk1, maintk2])]
        for tk1_, dep_, tk2_ in rel_triplets:
            if tk1_._.rel == "O":
                tk1_._.rel = "rel"
            if tk2_._.rel == "O":
                tk2_._.rel = "rel"

        for reltk1, reldep, reltk2 in rel_triplets:
            arguments = ["acl:recl", "dobj", "iobj", "xcomp", "ccomp", "advcl", "advmod", "name", "nmod", "amod"]
            args2_triplets = [((tk1_, dep_, tk2_)) for tk1_, dep_, tk2_ in triplets if dep_ in arguments and (tk1_ in [reltk1, reltk2] or tk2_ in [reltk1, reltk2])]
            for tk1_, dep_, tk2_ in args2_triplets:
                if tk1_._.rel == "O":
                    tk1_._.rel = "subj"
                if tk2_._.rel == "O":
                    tk2_._.rel = "subj"

        complements = ["mark", "det", "name", "amod", "nmod", "dep"]
        comp_triplets = [((tk1_, dep_, tk2_)) for tk1_, dep_, tk2_ in triplets if dep_ in complements and ("subj" in tk1_._.rel or "subj" in tk2_._.rel)]
        for tk1_, dep_, tk2_ in comp_triplets:
            if tk1_._.rel == "O":
                tk1_._.rel = "subjc"
            if tk2_._.rel == "O":
                tk2_._.rel = "subjc"

    
    for token in doc:
        rid1, rtag1 = (token.i, token._.rel)
        rid2, rtag2 = (token.i+1, None)
        while rid2 < len(doc) and doc[rid2]._.rel == "O":
            rid2 = rid2 + 1
        if rid2 < len(doc):
            rtag2 = doc[rid2]._.rel
            if "subj" in rtag1 and "subj" in rtag2:
                for rid_ in range(rid1, rid2):
                    if doc[rid_]._.rel == "O":
                        doc[rid_]._.rel= "subjc"
            if "rel" in rtag1 and "rel" in rtag2:
                for rid_ in range(rid1, rid2):
                    if doc[rid_]._.rel == "O":
                        doc[rid_]._.rel = "rel"
                        

    print(" ".join([tk.text+"_"+tk._.rel for tk in doc]))

    rel_links = [(None, tk.i, tk.head.i) if tk.i < tk.head.i else (tk.head.i, tk.i, None) for tk in doc if tk._.rel == "rel" and tk.head._.rel == "subj"] + \
                [(tk.i, tk.head.i, None) if tk.i < tk.head.i else (None, tk.head.i, tk.i) for tk in doc if tk._.rel == "subj" and tk.head._.rel == "rel"] + \
                [(tk.i, None, tk.head.i) if tk.i < tk.head.i else (tk.head.i, None, tk.i) for tk in doc if tk.head._.rel == "subj" and tk._.rel == "subj"]

    relidx = []
    for rid, rtri1 in enumerate(rel_links):
        for rtri2 in rel_links[rid:]:
            if rtri1.index(None) != rtri2.index(None):
                if len(list(set(rtri1) & set(rtri2))) > 1:
                    reltriple = [None, None, None]
                    reltriple[rtri2.index(None)] = list(rtri1)[rtri2.index(None)]
                    reltriple[rtri1.index(None)] = list(rtri2)[rtri1.index(None)]
                    reltriple[reltriple.index(None)] = [r for r in list((set(rtri1) & set(rtri2))) if r != None][0]
                    a1, r, a2 = reltriple
                    if a1 < r and r < a2:
                        relidx.append(reltriple)
    relations = []
    for a1, r, a2 in relidx:
        triple = []
        for idx in [a1, r, a2]:
            indexes = ((idx, idx+1))
            diff = 0
            while indexes[0]+diff >= 0 and doc[idx]._.rel in doc[idx+diff]._.rel:
                indexes=(idx+diff, indexes[1])
                diff=diff-1
            diff = 0
            while idx+diff < len(doc) and doc[idx]._.rel in doc[idx+diff]._.rel:
                indexes=(indexes[0], idx+diff)
                diff=diff+1
            triple.append(indexes)
        relations.append([doc[t1:t2+1].text for t1, t2 in triple])
    print(relations)    
    return relations

def pattern_match(pattern, sequence):
    print(pattern, sequence)
    k = len(pattern)
    i = itertools.tee(sequence, k)
    [next(i[j]) for j in range(k) for _ in range(j)]
    for lid, sublist in enumerate(zip(*i)):
        print(list(sublist), pattern)
        if list(sublist) == pattern:
            return lid

def relation_learning(sentence, triples):
    nlp = spacy.load("pt_core_news_sm")
    #nlp = spacy.load("pt")
    tokenizer = nlp.Defaults.create_tokenizer(nlp)
    doc = nlp(sentence)
    token = doc[0]
    
    dependency = [(token.text, token.dep_) for token in doc]
    token.set_extension("rel", default="O")#, force=True)

    print(sentence)

    for t in triples:
        arg1, rel, arg2 = t
        argid = []
        auxtk = [t.text for t in nlp(arg1)]
        tks = pattern_match(auxtk, [token.text for token in doc])
        tk_arg1 = (tks, tks+len(auxtk))
        auxtk = [t.text for t in nlp(arg2)]
        tks = pattern_match(auxtk, [token.text for token in doc])
        tk_arg2 = (tks, tks+len(auxtk))
        # if tk_arg1[0] < tk_arg2[0]:
        #     print([token.text for token in doc][tk_arg1[0]:tk_arg2[0]], tk_arg1[0],tk_arg2[0])
        #     tks = tks = pattern_match(rel.split(), [token.text for token in doc][tk_arg1[0]:tk_arg2[0]])
        #     tks_rel = (tk_arg1[0]+tks, tk_arg1[0]+tks+len(rel.split()))
        # else:
        #     tks = tks = pattern_match(rel.split(), [token.text for token in doc][tk_arg2[0]:tk_arg1[0]])
        #     tks_rel = (tk_arg2[0]+tks, tk_arg2[0]+tks+len(rel.split()))
        

        if tk_arg1 != [] and tk_arg2 != []:

            for tid in range(tk_arg1[0], tk_arg1[1]):
                doc[tid]._.rel = "arg1"

            # for tid in tk_rel:
            #     doc[tid]._.rel = "rel"

            for tid in range(tk_arg2[0], tk_arg2[1]):
                doc[tid]._.rel = "arg2"

        print([tk._.rel for tk in doc])
                
    # print(sentence)
    # print(dependency)
    # print(triples)

def read_train(doc, header=True, enc_="utf-8", path_="./"):
    data = ""
    head = []
    with open(path_+doc, "r", encoding=enc_) as docio:
        data = docio.read()
    
    data = data.split("\n")
    if header:
        head = data[0]
        data = data[1:]
    train = [d.split("\t") for d in data if d != ""]

    train = [(t[2], (t[3], t[4], t[5])) for t in train]
    sents = np.unique([t1 for t1, t1_ in train])
    for s in sents:
        yield str(s), [t_ for t, t_ in train if s == t]

def read_conll(doc, enc_="utf-8", path_="./"):
    data = ""
    texts = []
    marks = []
    phrases = []
    splits = []

    with open(path_+doc, "r", encoding=enc_) as docio:
        data = docio.read()
    
    data = data.split("\n")
    
    splits = [dataid for dataid, value in enumerate(data) if value.strip() == ""]

    si, sf = (0, 0)
    for s in splits:
        sf = s
        
        if data[si:sf] != []:
            phrases.append(data[si:sf])
        
        si = sf + 1
    
    if data[sf:] != [""]:
        phrases.append(data[sf:])
    
    #print([p for p in phrases])

    for p in phrases:
        items = [value.strip().split() for value in p]
        yield [t_ for t_, m_ in items], [m_ for t_, m_ in items]

def main():
    #assert spacy.__version__ == "2.0.0"

    
    """
    pathname = "./testes/"
    documents = []
    labels = []
    for docname in os.listdir(pathname):
        documents, labels = zip(*[d_ for d_ in read_conll(docname, path_=pathname)])

        # print(documents)
        # print(labels)

        for d, l in zip(documents, labels):
            dependencies, spacy_doc = dependency_parser(d, l)
            relations = relation_extraction(dependencies, spacy_doc)
            print(["Arg1 : "+a1+" | Rel : "+rel+" Arg2 : "+a2 for a1, rel, a2 in relations])
        # doc_inits = [s1.start() for s1 in re.finditer("<DOC DOCID=", documents)]
        # doc_ends = [s2.start() for s2 in re.finditer("</DOC>", documents)]
        
        # dlimits = [idx for idx in zip(doc_inits, doc_ends)]

        # documents = [documents[upper : lower] for upper, lower in dlimits]
        
        # for d in documents:
        #     dependency_parser(d)
    """
    pathname = "./"
    docname = "IberLEFTask3-data.csv"
    sentences, triples = zip(*[d_ for d_ in read_train(docname, path_=pathname)])
    for s, t in zip(sentences, triples):
        relation_learning(s, t)


if __name__ == "__main__":
    main()